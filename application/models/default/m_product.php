<?php 
class M_product extends M_myweb
{
	private $where;
	private $order_by;
	private $page;
	//lay danh sach tat ca product
	public function __construct() {
		parent::__construct();
		//$this->table = 'member';
		if (!isset($this->where )) 
		   $this->where = array();
		   
		if(!isset($order_by)){
			$this->order_by=array();
		}
	}
	public function getProducts()
	{
		
		$this->db->select("	product.id,
							product.name,
							product.short_des,
							product.detail_des,
							product.category_id,
							product.producerId,
							product.img4,
							product.img3,
							product.img2,
							product.img1,
							product.event_id,
							product.price,
							product.promotion_id,
							product.inStock,
							product.deleted,
							product.slug,
							category.id as `category_id`,
							category.name as `category_name`,
							category.slug as `category_slug`");
		$this->db->where('product.deleted',0);
		$this->db->where('product.hot',1);
		$this->db->where('product.active',1);
		if($this->order_by)
		{
			foreach($this->order_by as $field => $order)
			{
				$this->db->order_by($field,$order);
			}
		}
		$this->db->from('product');
		$this->db->join('category','category.id = product.category_id');
		return $this->db->get()->result();
	}
	public function getProductWhere()
	{
		$this->db->select("	product.id,
							product.name,
							product.short_des,
							product.detail_des,
							product.category_id,
							product.producerId,
							product.img4,
							product.img3,
							product.img2,
							product.img1,
							product.event_id,
							product.price,
							product.promotion_id,
							product.inStock,
							product.deleted,
							product.slug,
							product.youtube,
							category.id as `category_id`,
							category.name as `category_name`,
							category.slug as `category_slug`");
		$this->db->where('product.deleted',0);
		$this->db->where($this->where);
		$this->db->from('product');
		$this->db->join('category','category.id = product.category_id');
		return $this->db->get()->row();
	}
	public function setWhere($where,$value)
	{
		$this->where[$where] = $value;
	}
	public function setOrderBy($field,$order="ASC")
	{
		$this->order_by[$field]=$order;
	}
	public function setPage($page)
	{
		$this->page = $page;
	}
}
?>