<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="shortcut icon" type="ico" href="assets/public/avatar/nguyenquanlogo.ico" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144497109-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-144497109-1');
    </script>
    <meta http-equiv=”content-language” content=”vi” />
    <meta http-equiv=”Content-Type” content=”text/html; charset=utf-8″ />
    <link href=”favicon.ico” rel=”shortcut icon” type=”image/x-icon” />
    <meta name='revisit-after' content='1 days' />
    <meta name=”robots” content=”all” />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ($this->uri->segment(1) == "san-pham") : ?>
    <title><?php echo isset($product) ? $product->name : ""; ?></title>
    <meta name=”description” content="<?php isset($product) ? $product->short_des : "" ?>" />
    <meta property="og:url" content="<?php echo site_url() . $this->uri->uri_string(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($product->name) ? $product->name : ""; ?>" />
    <meta property="og:description" content="<?php echo isset($product) ? $product->short_des : ""; ?>" />
    <meta property="og:image" content="<?php echo isset($product) ? base_url('assets/public/avatar/') . $product->img1 : ""; ?>" />
    <meta name="image" content="<?php echo isset($product) ? base_url('assets/public/avatar/') . $product->img1 : ""; ?>" />
    <meta property="og:image:width" content="700">
    <meta property="og:image:height" content="630">
    <meta property="fb:app_id" content="1650887995041303" />
    <?php elseif ($this->uri->segment(1) == "danh-muc-san-pham") : ?>
    <?php if (isset($meta)) : ?>
    <title><?php echo isset($meta) ? $meta['title'] : ""; ?></title>
    <meta name="description" content="<?php echo isset($meta) ? $meta['meta'] : "" ?>" />
    <?php else : ?>
    <title><?php echo isset($cates) ? $cates[0]->name : ""; ?></title>
    <meta name="description" content="<?php echo isset($cates) ? $cates->description : "" ?>" />
    <?php endif ?>
    <meta property="og:url" content="<?php echo site_url() . $this->uri->uri_string(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($product) ? $product->name : ""; ?>" />
    <meta property="og:description" content="<?php echo isset($product) ? $product->short_des : ""; ?>" />
    <meta property="og:image" content="<?php echo isset($product) ? base_url('assets/public/avatar/') . $product->img1 : ""; ?>" />
    <meta property="og:image:width" content="700">
    <meta property="og:image:height" content="630">
    <meta property="fb:app_id" content="1650887995041303" />
    <?php elseif ($this->uri->segment(1) == "ve-chung-toi") : ?>
    <title><?php echo isset($introduce_info) ? $introduce_info->title : ""; ?></title>
    <meta name="description" content="<?php echo isset($introduce_info) ? $introduce_info->short_des : "" ?>" />
    <?php elseif ($this->uri->segment(1) == "tin-tuc") : ?>
    <?php if (isset($news_data)) : ?>
    <title><?php echo isset($news_data) ? $news_data->name : ""; ?></title>
    <meta name="description" content="<?php echo isset($news_data) ? $news_data->description : "" ?>" />
    <?php elseif (isset($meta)) : ?>
    <title><?php echo isset($meta) ? $meta['title'] : ""; ?></title>
    <meta name="description" content="<?php echo isset($meta) ? $meta['meta'] : "" ?>" />
    <?php else : ?>
    <title><?php echo isset($cate) ? $cate->name : ""; ?></title>
    <meta name="description" content="<?php echo isset($cate) ? $cate->description : "" ?>" />
    <?php endif; ?>
    <meta property="og:url" content="<?php echo site_url() . $this->uri->uri_string(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($news_data) ? $news_data->name : ""; ?>" />
    <meta property="og:description" content="<?php echo isset($news_data) ? $news_data->description : ""; ?>" />
    <meta property="og:image" content="<?php echo isset($news_data) ? site_url('assets/public/avatar/') . $news_data->img : ""; ?>" />
    <meta property="og:image:width" content="700">
    <meta property="og:image:height" content="630">
    <meta property="fb:app_id" content="1650887995041303" />
    <?php elseif ($this->uri->segment(1) == "thu-vien" && isset($album)) : ?>
    <meta property="og:url" content="<?php echo site_url() . $this->uri->uri_string(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($gallery_data) ? $gallery_data->name : ""; ?>" />
    <meta property="og:description" content="<?php echo isset($gallery_data) ? $gallery_data->description : ""; ?>" />
    <meta property="og:image" content="<?php echo isset($images)||!empty($images) ? $images[0]->img : ""; ?>" />
    <meta property="og:image:width" content="700">
    <meta property="og:image:height" content="630">
    <meta property="fb:app_id" content="1650887995041303" />
    <?php elseif ($this->uri->segment(1) == "tim-kiem") : ?>
    <title><?php echo isset($meta) ? $meta['title'] : ""; ?></title>
    <meta name="description" content="<?php echo isset($meta) ? $meta['meta'] : "" ?>" />
    <?php else : ?>
    <title><?php echo $meta ? $meta['title'] : ""; ?></title>
    <meta name="description" content="<?php echo isset($meta) ? $meta['meta'] : "" ?>" />
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo base_url();?>statics/default/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>statics/default/fontawesome-free/css/all.css">
    <?php
    switch ($this->uri->segment(1)) {
        case 'danh-muc-san-pham':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/category.css") . '">';
            break;

        case 'tin-tuc':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/news.css") . '">
            <link rel="stylesheet" href="' . site_url("statics/default/css/news_detail.css") . '">';
            break;

        default:
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/about.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/index.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/category.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/partner.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/product.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/gallery.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/fancybox-master/dist/jquery.fancybox.min.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/news.css") . '"><link rel="stylesheet" href="' . site_url("statics/default/css/news_detail.css") . '">';
    }
    ?>
    <link rel="stylesheet" href="<?php echo site_url('statics/default/css/header.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/swiper/dist/css/swiper.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/perfect-scrollbar/css/perfect-scrollbar.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/owl/dist/assets/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/owl/dist/assets/owl.theme.default.min.css'); ?>">
    <link href="https://fonts.googleapis.com/css?family=Yeseva+One&display=swap&subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=vietnamese" rel="stylesheet">
    <style>
        body {
            font-family: 'Roboto', sans-serif;
        }
    </style>
    
</head>

<body>
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                xfbml: true,
                version: 'v3.3'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script src="https://sp.zalo.me/plugins/sdk.js"></script>
    <!-- Your customer chat code -->
    <div class="fb-customerchat" attribution=setup_tool page_id="460926988025435" theme_color="#ffc300" logged_in_greeting="Xin Chào, chúng tôi có thể giúp gì cho bạn?" logged_out_greeting="Xin Chào, chúng tôi có thể giúp gì cho bạn?">
    </div>

    <?php if ($this->uri->segment(1) == "san-pham" || $this->uri->segment(1) == "danh-muc-san-pham" || $this->uri->segment(1) == "tin-tuc") : ?>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=1650887995041303&autoLogAppEvents=1"></script>
    <?php endif; ?>
    <header>
        <nav class="navbar-expand-lg navbar-dark navbar-lg bg-white" >

            <div class="container navbar-desktop brand-bar">
            <div class="">
                <img src="<?php echo site_url('');?>assets/public/avatar/nguyenquanlogo.png" class="img-fluid" style="max-height:70px">
            </div>
                <div class='slogan'>
                    <a class="navbar-brand brand-md my-0 text-center" href="<?php echo site_url(); ?>">
                        Luxury Orchid Viet Nam
                    </a>
                    <p class="text-center slogan">Hồ điệp giá tốt nhất</p>
                </div>
            </div>

        </nav>
    </header>