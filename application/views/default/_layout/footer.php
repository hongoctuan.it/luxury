 <!-- Footer -->
 <footer class="footer-distributed">
        <div class="container footer-wrap">


            <div class="footer-left">

                <h3>Công Ty TNHH TM DV Nguyên Quân VN
                </h3>

                <p class="footer-links">
                    <a href="<?php echo site_url();?>">Trang Chủ</a>
                    ·
                    <a href="<?php echo site_url();?>danh-muc-san-pham">Sản Phẩm</a>
                    ·
                    <a href="<?php echo site_url();?>thu-vien">Hình Ảnh</a>
                    ·
                    <a href="<?php echo site_url();?>tin-tuc">Tin Tức</a>
                    ·
                    <a href="<?php echo site_url();?>ve-chung-toi">Về Chúng Tôi</a>
                    ·
                    <a href="<?php echo site_url();?>doi-tac">Đối Tác</a>

                </p>

                <p class="footer-company-name">NguyenQuan &copy; 2018</p>
            </div>


            <div class="footer-right" >
                <div class="row">
                    <div class="col-4 footer-icon">
                        <i class="fa fa-map-marker"></i>
                    </div>
                    <div class="col-8 mt-2 footer-title">
                        <p><?php echo $company_info?$company_info['address']->value:'';?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 footer-icon">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="col-8 mt-3 footer-title">
                        <p><?php echo $company_info?$company_info['phone']->value:'';?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 footer-icon">
                        <a href="<?php echo $company_info?$company_info['facebook']->value:'';?>">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </div>
                    <div class="col-8 mt-3 footer-title">
                        <p>
                            <a href="<?php echo $company_info?$company_info['facebook']->value:'';?>">Địa chỉ Facebook</a>
                        </p>
                    </div>

                </div>
            </div>
            <?php //print_r($company_info);?>
        </div>

    <div class="side-btn">
        <a href="<?php echo $company_info?$company_info['facebook']->value:'';?>">
            <img src="<?php echo site_url('statics/default/img/facebook_icon.png');?>" id="fixedbutton">
        </a>
        <a href="" class="modal-btn" data-toggle="modal" data-target="#exampleModal">
            <div class="partner-register">
                <div class="partner-register-circle text-center">
                    <i class="fas fa-handshake"></i>

                </div>
                <div class="partner-register-desc">Đăng ký đối tác</div>
            </div>
        </a>
    </div>
    <div class="modal fade modal-partner" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Đăng ký làm đối tác</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body register-partner">
                                <form>
                                    <input type="text" name="representator" class='partner-representator' required placeholder="Tên Người Đại Diện*" />
                                    <input type="text" name="company" class='partner-company' required placeholder="Tên Công Ty*" />
                                    <input type="email" name="email" class='partner-email' required placeholder="Email*" />
                                    <input type="phone" name="phone" class='partner-phone' required placeholder="Số ĐT*" />
                                    <hr>
                                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn partner-registry-btn">Send</button>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
    <button class="back-to-top-btn text-center" id="back-to-top-btn" data-toggle="tooltip" title="Back to Top!">
        <i class="fas fa-arrow-up"></i>
    </button>
    <script src="https://sp.zalo.me/plugins/sdk.js"></script>
    <script src="<?php echo site_url('statics/default/jquery/dist/jquery.js');?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="<?php echo base_url('statics/default/bootstrap/dist/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo site_url('statics/default/owl/dist/owl.carousel.min.js');?>"></script>
    <script src="<?php echo site_url('statics/default/animejs/anime.min.js');?>"></script>
    <script src="<?php echo site_url('statics/default/fancybox-master/dist/jquery.fancybox.js');?>"></script>
    <script src="<?php echo site_url('statics/default/masonry/imagesloaded.pkgd.js');?>"></script>
    <script src="<?php echo site_url('statics/default/masonry/isotope.pkgd.js');?>"></script>
    <script src="<?php echo site_url('statics/default/swiper/dist/js/swiper.min.js');?>"></script>
    <?php $this->load->view('default/_layout/scripts');?>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    <?php if($this->uri->segment(1)=="danh-muc-san-pham"):?>
        $('body').on('click',".load-more", function()
        {
            $('.load-more').fadeOut();
            var category = <?php echo isset($category_id)?$category_id:0;?>;
            var page = $('.current-page').val();
            var stopped = $('.stopped').val();
            var style = '<?php echo isset($_GET['style'])?$_GET['style']:"grid";?>';
            var order = '<?php echo isset($_GET['order'])?$_GET['order']:false;?>';
            var by = '<?php echo isset($_GET['by'])?$_GET['by']:false;?>';
            $element = $('.product-list-content');
            if (stopped == 1){
                return false;
            }
            page++;
            $.ajax(
            {
                method        : 'POST',
                dataType    : 'text',
                url         : '<?php echo site_url("/category/ajax/getpage");?>',
                data        : { page : page,
                                category : category,
                                style : style,
                                order : order,
                                by : by},
                success     : function (result)
                {
                    if(result)
                    {
                        $('.current-page').val(page);
                        $element.append($(result).hide().fadeIn(500));
                        $('.load-more').fadeIn(500);
                        setTimeout(
                            function() 
                            {
                                $('.lds-spinner').fadeOut();
                                setTimeout(
                                    function() 
                                    {
                                        $('.product-image').delay().addClass('show');
                                        $('.featured-products-img').delay().addClass('show');
                                        $('.category-product-item-img img').addClass('show');
                                        $('.category-sidebar-hot-products-item img').addClass('show');
                                        $('.banner-image').addClass('show');
                                        }, 500);
                            }, 1500);
                    }else{
                        $('.stopped').val(1);
                        $element.append($('<div class=col-12><p class=text-center><b>không còn sản phẩm</b></p></div>').hide().fadeIn(500));
                        $('.load-more').fadeOut(500);
                    }
                
                }
            })

        });
    <?php endif;?>

    <?php if($this->uri->segment(1)=="thu-vien"):?>
    $(document).ready(function(){
    $('body').on('click',".gallery-see-more", function()
        {
            $('.gallery-see-more').fadeOut(500);
            var category = <?php echo isset($gallery_data)?$gallery_data->id:0;?>;
            var page = $('.current-page').val();
            var stopped = $('.stopped').val();
            $element = $('.grid');
            if (stopped == 1){
                return false;
            }
            page++;
            $.ajax(
            {
                method        : 'POST',
                dataType    : 'text',
                url         : '<?php echo site_url("/gallery/ajax/getpage");?>',
                data        : {page : page,
                                category : category},
                success     : function (result)
                {
                    if(result)
                    {
                        $('.current-page').val(page);
                        $element.isotope( 'insert',$(result).hide().fadeIn(500));
                        $('.grid').imagesLoaded().progress(function () { 
                            $('.grid').isotope('layout');
                        });
                        $('.gallery-see-more').delay(1000).fadeIn(500);
                    }else{
                        $('.stopped').val(1);
                        $element.after($('<div class=col-12><p class=text-center><b>không còn hình ảnh</b></p></div>').hide().fadeIn(500));
                        $('.gallery-see-more').fadeOut(500);
                    }
                
                }
            });
        });
    });
    <?php endif;?>


        $(document).ready(function(){
        $('body').on('click',".partner-registry-btn", function()
            {
                var representator = $('.partner-representator').val();
                var company = $('.partner-company').val();
                var phone = $('.partner-phone').val();
                var email = $('.partner-email').val();
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if(representator&&company&&phone&&email)
                {
                    if(re.test(email)){
                        $.ajax(
                        {
                            method        : 'POST',
                            dataType    : 'text',
                            url         : '<?php echo site_url("/main/partner_registry");?>',
                            data        : {representator : representator,
                                            company : company,
                                            phone : phone,
                                            email : email},
                            success     : function (result)
                            {
                                console.log(result);
                                if(result==1)
                                {
                                    if($('.error-msg'))
                                    {
                                        $('.error-msg').fadeOut();
                                        $('.register-partner').fadeOut(500);
                                        $('.modal-content').append('<p class=text-center><b>Thông tin đã được gửi thành công</b></p>');
                                    }else{
                                        $('.register-partner').fadeOut(500);
                                        $('.modal-content').append('<p class=text-center><b>Thông tin đã được gửi thành công</b></p>');
                                    }
                                }else{
                                    $('.modal-content').append('<p class=text-center style="color:red"><b>Đã có lỗi xảy ra</b></p>');
                                }
                            
                            }
                        });
                    }else{
                        if($('.error-msg'))
                        {
                            $('.error-msg').fadeOut();
                            $('.modal-content').delay(2000).fadeIn().append('<p class="text-center error-msg" style="color:red;"><b>Vui lòng điền đúng email</b></p>');
                            $('.error-msg').delay(4000).fadeOut();
                        }
                        else{
                            $('.modal-content').append('<p class="text-center error-msg" style="color:red;"><b>Vui lòng điền đúng email</b></p>');
                            $('.error-msg').delay(4000).fadeOut();
                        }
                    }
                }else{
                    if($('.error-msg'))
                    {
                        $('.error-msg').fadeOut();
                        $('.modal-content').delay(2000).fadeIn().append('<p class="text-center error-msg" style="color:red;"><b>Vui lòng điền đầy đủ thông tin</b></p>');
                        $('.error-msg').delay(4000).fadeOut();
                    }
                    else{
                        $('.modal-content').append('<p class="text-center error-msg" style="color:red;"><b>Vui lòng điền đầy đủ thông tin</b></p>');
                        $('.error-msg').delay(4000).fadeOut();
                    }

                }

            });
    <?php if($this->uri->segment(1)=='danh-muc-san-pham' || $this->uri->segment(1)=='tin-tuc'|| $this->uri->segment(1)=='thu-vien'):?>
        $('body').on('click',".share-fb", function()
        {
            <?php if($this->uri->segment(1)=='danh-muc-san-pham'):?>
            var link = $(this).closest('figure').find('.product-wrap-link').attr('href');
            <?php elseif($this->uri->segment(1)=='tin-tuc'):?>
            var link = $(this).siblings('.news-overview-read-more').attr('href');
            <?php elseif($this->uri->segment(1)=='thu-vien'):?>
            var link = $(this).siblings('a').attr('href');
            <?php elseif($this->uri->segment(1)=='san-pham'):?>
            var link = <?php echo current_url();?>
            <?php endif;?>
            link = "https://www.facebook.com/sharer/sharer.php?u="+link;
            var fbpopup = window.open(link, "pop", "width=600, height=400, scrollbars=no");
            return false;
        });

    <?php endif;?>
    
    <?php if($this->uri->segment(1)=='doi-tac'||$this->uri->segment(1)=='san-pham'):?>
        $('body').on('click','.shops-wrap',function(){
            $(this).parent('.shops-list').find(".active").removeClass('active');
            var map_src=$(this).find('.map_src').val();
            if(map_src.length<10){
                var iframe = "<p class='google-map-iframe'>không có dữ liệu</p>";
            }else{
                var iframe = '<iframe class="google-map-iframe" src="https://'+map_src+'" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>'
            }
            $(this).parents('.tab-pane').find(".google-map-iframe").remove();
            $(this).parents('.tab-pane').find(".google-map-iframe-wrap").append(iframe);
            $(this).addClass('active');
        });
    <?php endif;?>
    });
    <?php if($this->uri->segment(1)=='san-pham'):?>
    function share_fb_function()
    {
        var link = window.location.href;
        link = "https://www.facebook.com/sharer/sharer.php?u="+link;
        var fbpopup = window.open(link, "pop", "width=600, height=400, scrollbars=no");
    }
    <?php endif;?>
    </script>
</body>

</html>