
    <section class="news-banner">

        <div class="news-banner-wrap text-center">

            <h1 class="news-banner-title text-white text-center">
               <?php echo isset($cate)?$cate->name:"Tin Tức";?>
            </h1>
        </div>

    </section>

    <section class="news-main my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-3 text-center text-md-left">
                    <div class="news-categories">
                        <a href="<?php echo site_url('news');?>" class=text-dark>
                            <h2 class="news-categories-title">Danh Mục Tin Tức</h2>
                        </a>
                        <ul class="news-categories-list">
                        <?php foreach($news_categories as $news_category):?>
                            <li class="news-categories-list-item <?php echo isset($slug)&&$news_category->slug==$slug?"active":'';?>">
                                <a href="<?php echo site_url('tin-tuc/danh-muc/').$news_category->slug;?>" class=text-muted><?php echo $news_category?$news_category->name:'';?></a>
                            </li>
                        <?php endforeach;?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 news-item-list">
                <?php foreach($news_datas as $news_data):?>
                    <div class="row news-item mb-5">
                        <div class="col-md-6">
                            <div class="news-image">
                                <div class="news-date">
                                    <p><?php echo date('d',strtotime($news_data->created_at));?>
                                        <span>Th <?php echo date('m',strtotime($news_data->created_at));?></span>
                                    </p>
                                </div>
                                <a style="background-image:url('<?php echo site_url('assets/public/avatar/').$news_data->img;?>')" class="news-image-wrap"></a>
                                <a href="<?php echo site_url('tin-tuc/').$news_data->slug;?>" class="read-more">Đọc Thêm</a>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="news-overview py-3">
                                <div class="news-overview-categories">
                                    <a href="<?php echo site_url('tin-tuc/danh-muc/').$news_data->category['slug'];?>" class="news-overview-categories-item">
                                        <?php echo $news_data->category['name'];?>
                                    </a>
                                </div>
                                <h2 class="news-overview-title">
                                    <?php echo $news_data->name;?>
                                </h2>
                                <p class="news-overview-author">
                                    <?php echo $news_data->created_by;?>
                                </p>
                                <p class="news-overview-description">
                                    <?php 
                                        $description = strip_tags($news_data->description); //Lược bỏ các tags HTML
                                        echo substr($description, 0, 450);
                                    ?>...
                                </p>
                                <button class="share-fb btn mr-3" style="z-index:10;background:#3b5998;font-size:20px;color:white;padding:0px 7px ;line-height:30px;height:30px"><i class="fab fa-facebook"></i> <span style="font-size:18px;margin:0px;font-weight:500;margin-bottom:2px;margin-left:3px"> Share</span></button> <a href="<?php echo site_url('tin-tuc/').$news_data->slug;?>" class="ml-3 news-overview-read-more btn ml-auto">ĐỌC THÊM</a>
                                
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
                </div>
            </div>

            <nav aria-label="Page">
                <ul class="pagination justify-content-center">
                    <li class="page-item <?php echo $this->page==1?"disabled":"";?>">
                        <a class="page-link text-dark" href="<?php echo site_url("tin-tuc/danh-muc");echo isset($slug)?"/".$slug:"";echo "?page=".($current_page-1);?>" tabindex="-1">
                            Trước</a>
                    </li>
                    <?php if($current_page >1):?>
                    <li class="page-item">
                        <a class="page-link page text-dark" href="<?php echo site_url("tin-tuc/danh-muc");echo isset($slug)?"/".$slug:""; echo "?page=".($current_page-1);?>"><?php echo $current_page - 1;?></a>
                    </li>
                    <?php endif;?>
                    <li class="page-item" >
                        <!-- <a class="page-link page-active text-dark"  href="<?php echo site_url("tin-tuc/danh-muc");echo isset($slug)?"/".$slug:""; echo "?page=".$current_page;?>"></a> -->
                        <p class="page-link page-active"><?php echo $current_page;?></p>
                    </li>
                    <?php if($current_page < $total_pages):?>
                    <li class="page-item">
                        <a class="page-link page text-dark" href="<?php echo site_url("tin-tuc/danh-muc");echo isset($slug)?"/".$slug:""; echo "?page=".($current_page+1);?>"><?php echo $current_page + 1;?></a>
                    </li>
                    <?php endif;?>
                    <li class="page-item <?php echo $this->page>=$total_pages?"disabled":"";?>">
                        <a class="page-link text-dark" href="<?php echo site_url("tin-tuc/danh-muc");echo isset($slug)?"/".$slug:""; echo "?page=".($current_page+1);?>">Sau</a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>
