<?php if ($style == 'grid' || !$style) : ?>
    <?php foreach ($products as $item) : ?>
        <div class="col-md-4 category-product-item text-center my-3">
            <!-- <a href="<?php echo site_url('san-pham?id=' . $item->id); ?>" -->
            <figure class="figure category-product-figure-grid">
                <a href="<?php echo site_url('san-pham/' . $item->slug); ?>" class="text-dark product-wrap-link">
                    <div class="category-product-item-img">
                    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                        <img src="<?php echo base_url(); ?>assets/public_thumbs/avatar/<?php echo $item->img1 ?>" alt="" class="img-fluid">
                    </div>
                </a>
                <figcaption class="p-2 k-flex align-items-end">
                    <h2 class="category-product-item-title"><?php echo $item->name ?></h2>
                    <a href="<?php echo site_url('danh-muc-san-pham/') . $item->category_slug; ?>" class="btn" style="font-size:16px;color:orange;padding:0px 5px;line-height:24px;height:24px;right:10px;"> <span style="font-size:14px;margin:0px;font-weight:500;margin-bottom:2px;margin-left:3px"> <?php echo $item->category_name; ?></span></a>
                    <?php if ($item->price != 0) { ?>
                        <p class="" style="color:grey;line-height:24px"> <span style="font-size:14px;margin:0px;font-weight:400;margin-bottom:2px;margin-left:3px"> <?php echo number_format($item->price, 0, ',', '.'); ?> VND</span></p>
                    <?php } ?>
                    <button class="share-fb btn" style="z-index:10;background:#3b5998;font-size:14px;color:white;padding:0px 5px ;line-height:20px;height:20px;bottom:33px;left:50%;transform:translateX(-50%)"><i class="fab fa-facebook"></i> <span style="line-height:20px;font-size:11px;margin:0px;font-weight:400;margin-bottom:2px;margin-left:3px"> Chia Sẻ</span></button>
                    <div class="zalo-share-button" data-href="<?php echo site_url('san-pham/'.$item->slug);?>" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false style="position:absolute;bottom:5px;left:50%;transform:translateX(-50%)"></div>
                </figcaption>
            </figure>

        </div>
    <?php endforeach; ?>
<?php elseif ($style == 'list') : ?>
    <?php foreach ($products as $item) : ?>
        <div class="category-product-item text-center my-2 col-sm-5">
            <!-- <a href="<?php echo site_url('san-pham?id=' . $item->id); ?>" -->
            <a href="<?php echo site_url('san-pham/' . $item->slug); ?>" class="text-dark product-wrap-link">
                <figure class="figure category-product-figure">
                    <div class="category-product-item-img">
                    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                        <img src="<?php echo site_url(); ?>assets/public_thumbs/avatar/<?php echo $item->img1 ?>" alt="" class="img-fluid">
                    </div>
                </figure>
            </a>
        </div>
        <div class="col-sm-7 category-product-item-text border-bottom my-2">
            <h2 class="category-product-item-title"><?php echo $item->name ?></h2>
            <?php if ($item->price != 0) { ?>
                <p class="mb-0" style="color:orange;line-height:24px"> <span style="font-size:14px;margin:0px;font-weight:500;margin-bottom:2px;margin-left:3px"> <?php echo number_format($item->price, 0, ',', '.'); ?> VND</span></p>
            <?php } ?>

            <a href="<?php echo site_url('danh-muc-san-pham/') . $item->category_slug; ?>" class="btn" style="background:orange;font-size:16px;color:white;padding:0px 5px;line-height:24px;height:24px;"> <span style="font-size:14px;margin:0px;font-weight:500;margin-bottom:2px;margin-left:3px"> <?php echo $item->category_name; ?></span></a>
            <!-- <p class="mb-0"><small class=pl-2 style="color:green;"><b>Quy Cách</b>: 12 thùng</small></p> -->
            <p class="category-prodcut-item-desc"><?php echo $item->short_des ?></p>
            <div>
                <div class="zalo-share-button" data-href="<?php echo site_url('san-pham/' . $item->slug); ?>" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false style="margin-top:50px;"></div>
                <button class="share-fb btn" style="z-index:10;background:#3b5998;font-size:12px;color:white;padding:0px 5px ;line-height:20px;height:20px;position:relative;margin-top:29px"><i class="fab fa-facebook"></i> <span style="font-size:12px;margin:0px;font-weight:400;margin-bottom:3px;margin-left:3px"> Chia Sẻ</span></button>
                
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>