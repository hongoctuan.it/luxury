<section class="category-banner">
    <div class="category-banner-wrap text-center">
        <ul class="category-list nav nav-pills mx-auto mt-lg-5" id="pills-tab" role="tablist">
                    <li class="category-list-item ">
                        <a class="nav-link category-list-item-link"
                            href="#banggiahodiep">
                            <div class="row">
                                <div>
                                    <h2 class="category-list-item-title text-left text-white active mr-1">
                                        Bảng giá Hồ điệp</h2>
                                </div>
                            </div>
                        </a>
                        <a class="nav-link category-list-item-link"
                            href="#banggiahodiep">
                            <div class="row">
                                <div>
                                    <h2 class="category-list-item-title text-left text-white active mr-1">
                                        Kỹ thuật chăm sóc</h2>
                                </div>
                            </div>
                        </a>
                    </li>
            </ul>
    </div>
</section>

<section class="category-product my-3" id="banggiahodiep" >
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="product-list">
                    <input type="hidden" value="1" class="current-page">
                    <input type="hidden" value="0" class="stopped">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="category-hot-products">
                            <ul class="category-sidebar-hot-products">
                                    <li class="category-sidebar-hot-products-item">
                                            <div class="row">
                                                <h4 class="" style="font-size:.9rem;color:#3f3f3f;">
                                                    HỒ ĐIỆP ĐẠI
                                                </h4>
                                                
                                            </div>
                                    </li>
                                    <li class="category-sidebar-hot-products-item">
                                        <div class="row">
                                                <h4 class="" style="font-size:.9rem;color:#3f3f3f;">
                                                    HỒ ĐIỆP MINI
                                                </h4>
                                        </div>
                                    </li>
                            </ul>
                        </div>
                        </div>

                        <div class="col-lg-9">
                            <div class="row product-list-content mt-2">
                                <div class="titlesection" id="hodiepdai">
                                    Hồ Điệp Đại
                                </div>
                            </div>
                            <div class="row product-list-content mt-2">
                                <div class="titlesection" id="hodiepmini">
                                    Hồ Điệp Mini
                                </div>
                            </div>
                            <div class="row product-list-content mt-2">
                                <div class="titlesection">
                                    Kỹ thuật chăm sóc
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>