<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<?php
$action = $obj ? site_url('admin/product?act=upd&id=' . $obj->id . "&token=" . $infoLog->token) : site_url('admin/product?act=upd&token=' . $infoLog->token);
$image_01 = $obj && $obj->img1 != "" ? base_url('assets/public/avatar/' . $obj->img1) : base_url('assets/public/avatar/no-avatar.png');
$image_02 = $obj && $obj->img2 != "" ? base_url('assets/public/avatar/' . $obj->img2) : base_url('assets/public/avatar/no-avatar.png');
$image_03 = $obj && $obj->img3 != "" ? base_url('assets/public/avatar/' . $obj->img3) : base_url('assets/public/avatar/no-avatar.png');
$image_04 = $obj && $obj->img4 != "" ? base_url('assets/public/avatar/' . $obj->img4) : base_url('assets/public/avatar/no-avatar.png');
?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Nguyên Quân / Sản phẩm
				</h6>
				<h3 class="dashhead-title">Danh sách sản phẩm</h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					Sản phẩm
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">
		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if (isset($_SESSION['system_msg'])) {
						echo $_SESSION['system_msg'];
						unset($_SESSION['system_msg']);
					} ?>
					<div class="row">
						<?php echo form_open_multipart($action, array('autocomplete' => "off", 'id' => "userform")); ?>
						<input type="hidden" id="id" name="id" value="<?php echo $obj ? $id : "" ?>">
						<div class="col-md-3">
							<div class="form-group required">
								<label class="control-label">Tên Sản Phẩm</label>
								<input type="text" class="form-control" name="name" id="name" required value="<?php echo $obj ? $obj->name : ""; ?>" />
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group required">
								<label class="control-label">Giá Sản Phẩm</label>
								<input type="text" class="form-control" name="price" id="price" required value="<?php echo $obj ? $obj->price : ""; ?>" />
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Loại Sản Phẩm</label>
								<select name="category_id" class="form-control">
									<?php
									$arr = array();
									foreach ($cates as $key) {
										if ($key->level == 0) {
											$arr[] = $key;
											foreach ($cates as $key2) {
												if ($key2->level == 1 && $key->id == $key2->parent) {
													$arr[] = $key2;
													foreach ($cates as $key3) {
														if ($key3->level == 2 && $key2->id == $key3->parent) {
															$arr[] = $key3;
														}
													}
												}
											}
										}
									}
									?>
									<?php foreach ($arr as $key) : ?>
										<option value="<?php echo $key->id ?>" <?php echo $obj && $obj->category_id == $key->id ? "selected" : "" ?>><?php if ($key->level == 1) echo '&nbsp;&nbsp;&nbsp;-';
										else if ($key->level == 2) echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+';
										echo $key->name ?></option>
									<?php endforeach; ?>


								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Slug (có thể bỏ trống)</label>
								<input type="text" class="form-control" name="slug" id="slug" value="<?php echo $obj ? $obj->slug : ""; ?>" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group required">
								<label class="control-label">Mô tả ngắn</label>
								<textarea required id="content" class="form-control" name="short_des" id="short_des"><?php echo $obj ? $obj->short_des : ""; ?></textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group required">
								<label class="control-label">Youtube</label>
								<input type="text" class="form-control" name="youtube" id="youtube" value="<?php echo $obj ? $obj->youtube : ""; ?>" />
							</div>
						</div>

						<div class="col-md-12">
						<label class="control-label">Danh sách cửa hàng</label><br>
							<?php if ($partner) : ?>
								<?php foreach ($partner as $item) : ?>
									<input type="checkbox" name="partner[]" value="<?php echo $item->id; ?>" <?php if (isset($product_partner)&&$product_partner) : ?> <?php foreach ($product_partner as $check) : ?> <?php if ($check->partner_id == $item->id) : ?> checked <?php endif; ?> <?php endforeach; ?> <?php endif; ?>> <?php echo $item->name; ?>|
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
						<div class="col-md-12">
							<div class="form-group required">
								<label class="control-label">Mô tả chi tiết</label>
								<textarea required id="content" class="form-control" name="detail_des" id="short_des"><?php echo $obj ? $obj->detail_des : ""; ?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('detail_des', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Hình 4</label>
								<div>
									<img id="imgFile_04" class="imgFile" alt="Avatar" src="<?php echo $image_04 ?>" />
									<input type="file" name="image_04" id="chooseImgFile" onchange="document.getElementById('imgFile_04').src = window.URL.createObjectURL(this.files[0])">
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Hình 1 (hình đại diện)</label>
								<div>
									<img id="imgFile_01" class="imgFile" alt="Avatar" src="<?php echo $image_01 ?>" />
									<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Hình 2</label>
								<div>
									<img id="imgFile_02" class="imgFile" alt="Avatar" src="<?php echo $image_02 ?>" />
									<input type="file" name="image_02" id="chooseImgFile" onchange="document.getElementById('imgFile_02').src = window.URL.createObjectURL(this.files[0])">
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Hình 3</label>
								<div>
									<img id="imgFile_03" class="imgFile" alt="Avatar" src="<?php echo $image_03 ?>" />
									<input type="file" name="image_03" id="chooseImgFile" onchange="document.getElementById('imgFile_03').src = window.URL.createObjectURL(this.files[0])">
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
                        <div class="row" style="text-align: center;">
							<div class="col-md-3">
								<div class="form-group">
									<a href="<?php echo $image_04 ?>" download><i class="fa fa-download"></i> Download</a>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<a href="<?php echo $image_01 ?>" download><i class="fa fa-download"></i> Download</a>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<a href="<?php echo $image_02 ?>" download><i class="fa fa-download"></i> Download</a>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<a href="<?php echo $image_03 ?>" download><i class="fa fa-download"></i> Download</a>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-3">
							<a class="btn btn-default" href="<?php echo site_url('admin/product'); ?>">Quay lại</a>
							<button type="reset" class="btn btn-warning">Huỷ</button>
							<button type="submit" id="formSubmit" class="btn btn-primary">Gửi</button>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->


</div>
<!-- END: .app-main -->