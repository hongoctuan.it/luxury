<?php
if(isset($parent)){
	$action = site_url('admin/category?act=upd&parent='.$parent."&token=".$infoLog->token);
}else{
	$action = $obj?site_url('admin/category?act=upd&id='.$obj->id."&token=".$infoLog->token):site_url('admin/category?act=upd&token='.$infoLog->token);
}
$image_01 = $obj&&$obj->img!=""?base_url('assets/public/avatar/'.$obj->img):base_url('assets/public/avatar/no-avatar.png');
$image_02 = $obj&&$obj->img_large!=""?base_url('assets/public/avatar/'.$obj->img_large):base_url('assets/public/avatar/no-avatar.png');

?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Nguyên Quân / Danh mục <strong><?php if(isset($cat_parent)) echo $cat_parent->name ?> </strong>
				</h6>
				<h3 class="dashhead-title">Danh mục <strong><?php if(isset($cat_parent)) echo $cat_parent->name ?> </strong></h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					Danh mục <strong><?php if(isset($cat_parent)) echo $cat_parent->name ?> </strong>
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<input type="hidden" id="id" name="id" value="<?php echo $obj?$id:"" ?>">
							<input type="hidden" id="level" name="level" value="<?php echo $obj?$obj->level:"" ?>">
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Tên danh mục</label>
									<input type="text" class="form-control" name="name" id="name" required value="<?php echo $obj?$obj->name:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Slug</label>
									<input type="text" class="form-control" name="slug" id="slug" disabled value="<?php echo $obj?$obj->slug:"";?>"/>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Ảnh đại diện
									</label>
									<div>
										<img id="imgFile_01" class="imgFile" alt="Avatar" src="<?php echo $image_01?>" />
										<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="form-group">
									<label>Hình ảnh (Hình ảnh danh mục hiển thị đẹp nhất với kích thước: ngang 754px x cao 300px và 374px x 300px)</label>
									<div>
										<img id="imgFile_02" class="imgFile" alt="Avatar" src="<?php echo $image_02?>" />
										<input type="file" name="image_02" id="chooseImgFile" onchange="document.getElementById('imgFile_02').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group required">
									<label class="control-label">Mô tả ngắn</label>
									<textarea required id="content" class="form-control" name="description" required id="description" ><?php echo $obj?$obj->description:"";?></textarea>
								</div>
								<script>
									var editor = CKEDITOR.replace('description',{
										language:'vi',
										filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

										filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
										
										filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
										
										filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
										
										filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
										
										filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

									});
                        		</script>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin/user');?>">Quay lại</a>
								<button type="reset" class="btn btn-warning">Huỷ</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Lưu</button>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->

	
</div>
<!-- END: .app-main -->