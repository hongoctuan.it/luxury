<?php
	$action = site_url('admin/hotproductinfo?act=upd&token='.$infoLog->token);
	$image_01 = $obj&&$obj->img!=""?base_url('assets/public/avatar/'.$obj->img):base_url('assets/public/avatar/no-avatar.png');
?>
<!-- <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); // convert all text areas to rich text editor on that page

</script> -->
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Nguyên Quân / Mô tả sản phẩm nổi bật
				</h6>
				<h3 class="dashhead-title">Mô tả sản phẩm nổi bật</h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					Mô tả sản phẩm nổi bật
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<div class="col-md-6">
								<div class="form-group">
									<label>Hình ảnh (Hình ảnh hiển thị đẹp nhất với kích thước: ngang 190px x cao 300px)</label>
									<div>
										<img id="imgFile_01" class="imgFile" alt="Avatar" src="<?php echo $image_01?>" />
										<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group required">
									<label class="control-label">Mô tả</label>
									<textarea id="content" class="form-control" name="descriptionfull" required id="descriptionfull" ><?php echo $obj?$obj->descriptionfull:"";?></textarea>
								</div>
								<script>
									CKEDITOR.replace( 'descriptionfull', {
										toolbar: [
											{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
											[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
											{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
										]
									});
                        		</script>
							</div>
							
							 <div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Điểm nổi bật 1</label>
									<textarea id="content" class="form-control" name="descriptionfull01" required id="descriptionfull01" ><?php echo $obj?$obj->descriptionfull01:"";?></textarea>
								</div>
								<script>
									var editor = CKEDITOR.replace('descriptionfull01',{
										language:'vi',
										toolbar: [
											{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
											[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
											{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
										],
										height:'100px'
									});
                        		</script>
							</div>
							
							 <div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Điểm nổi bật 2</label>
									<textarea id="content" class="form-control" name="descriptionfull02" required id="descriptionfull02" ><?php echo $obj?$obj->descriptionfull02:"";?></textarea>
								</div>
								<script>
									var editor = CKEDITOR.replace('descriptionfull02',{
										language:'vi',
										toolbar: [
											{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
											[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
											{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
										],
										height:'100px'
									});
                        		</script>
							</div>

							 <div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Điểm nổi bật 3</label>
									<textarea id="content" class="form-control" name="descriptionfull03" required id="descriptionfull03" ><?php echo $obj?$obj->descriptionfull03:"";?></textarea>
								</div>
								<script>
									var editor = CKEDITOR.replace('descriptionfull03',{
										language:'vi',
										toolbar: [
											{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
											[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
											{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
										],
										height:'100px'
									});
                        		</script>
							</div>

							 <div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Điểm nổi bật 4</label>
									<textarea id="content" class="form-control" name="descriptionfull04" required id="descriptionfull04" ><?php echo $obj?$obj->descriptionfull04:"";?></textarea>
								</div>
								<script>
									var editor = CKEDITOR.replace('descriptionfull04',{
										language:'vi',
										toolbar: [
											{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
											[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
											{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
										],
										height:'100px'
									});
                        		</script>
							</div>

							 <div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Điểm nổi bật 5</label>
									<textarea id="content" class="form-control" name="descriptionfull05" required id="descriptionfull05" ><?php echo $obj?$obj->descriptionfull05:"";?></textarea>
								</div>
								<script>
									var editor = CKEDITOR.replace('descriptionfull05',{
										language:'vi',
										toolbar: [
											{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
											[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
											{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
										],
										height:'100px'
									});
                        		</script>
							</div>

							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Điểm nổi bật 4</label>
									<textarea id="content" class="form-control" name="descriptionfull06" required id="descriptionfull06" ><?php echo $obj?$obj->descriptionfull06:"";?></textarea>
								</div>
								<script>
									var editor = CKEDITOR.replace('descriptionfull06',{
										language:'vi',
										toolbar: [
											{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
											[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
											{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
										],
										height:'100px'
									});
                        		</script>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin/user');?>">Quay lại</a>
								<button type="reset" class="btn btn-warning">Huỷ</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Lưu</button>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->

	
</div>
<!-- END: .app-main -->