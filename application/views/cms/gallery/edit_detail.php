<?php
	$action = $obj?site_url('admin/gallery_detail?act=upd&id='.$obj->id."&token=".$infoLog->token):site_url('admin/gallery_detail?act=upd&gallery='.$gallery.'&token='.$infoLog->token);
	$image_01 = $obj&&$obj->img!=""?base_url('assets/public/avatar/'.$obj->img):base_url('assets/public/avatar/no-avatar.png');
?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Nguyên Quân / Hình ảnh
				</h6>
				<h3 class="dashhead-title">Hình ảnh</h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					Hình ảnh
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<input type="hidden" id="id" name="id" value="<?php echo $obj?$obj->id:"" ?>">
							<input type="hidden" id="gallery_id" name="gallery_id" value="<?php echo isset($_GET['gallery'])?$_GET['gallery']:"" ?>">
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Tên hình ảnh</label>
									<input type="text" class="form-control" name="name" id="name" required value="<?php echo $obj?$obj->name:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Slug</label>
									<input type="text" class="form-control" name="slug" id="slug" required value="<?php echo $obj?$obj->slug:"";?>"/>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Hình ảnh</label>
									<div>
										<img id="imgFile_01" class="imgFile img-fluid" alt="Avatar" src="<?php echo $image_01?>" />
										<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="col-md-9">
								<div class="form-group required">
									<label class="control-label">Mô tả</label>
									<textarea id="content" class="form-control" name="description" required id="description" ><?php echo $obj?$obj->description:"";?></textarea>
								</div>
								<script>
									var editor = CKEDITOR.replace('description',{
										language:'vi',
										filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

										filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
										
										filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
										
										filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
										
										filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
										
										filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

									});
                        		</script>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin/gallery_detail?id='.$_GET['gallery'].'&token='.$infoLog->token);?>">Trở lại</a>
								<button type="reset" class="btn btn-warning">Huỷ</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Gửi</button>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->

	
</div>
<!-- END: .app-main -->