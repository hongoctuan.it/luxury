<?php
if(isset($parent)){
	$action = site_url('admin/news?act=upd&parent='.$parent."&token=".$infoLog->token);
}else{
	$action = $obj?site_url('admin/news?act=upd&id='.$obj->id."&token=".$infoLog->token):site_url('admin/news?act=upd&token='.$infoLog->token);
}
$image_01 = $obj&&$obj->img!=""?base_url('assets/public/avatar/'.$obj->img):base_url('assets/public/avatar/no-avatar.png');
?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Nguyên Quân / <?php echo isset($parent)?"Tin tức":"Danh mục tin tức" ?>
				</h6>
				<h3 class="dashhead-title"><?php echo isset($parent)?"Tin tức":"Danh mục tin tức" ?></h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
				<?php echo isset($parent)?"Tin tức":"Danh mục tin tức" ?>
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<input type="hidden" id="id" name="id" value="<?php echo $obj?$id:"" ?>">
							<div class="col-md-12">
								<div class="form-group required">
									<label class="control-label">Tên tin tức</label>
									<input type="text" class="form-control" name="name" id="name" required value="<?php echo $obj?$obj->name:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Danh mục tin tức</label>
									<select name="parent" class="form-control">
										<option value="0">Root</option>
										<?php foreach($news_cate as $item): ?>
											<option value="<?php echo $item->id?>" <?php echo $obj&&$obj->parent==$item->id?"selected":""?>><?php echo $item->name?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Slug (có thể bỏ trống)</label>
									<input type="text" class="form-control" name="slug" id="slug" value="<?php echo $obj?$obj->slug:"";?>"/>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label>Hình ảnh tin tức (Hình ảnh hiển thị đẹp nhất với kích thước: ngang 397px x cao 280px)</label>
									<div>
										<img id="imgFile_01" class="imgFile" alt="Avatar" src="<?php echo $image_01?>" />
										<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group required">
									<label class="control-label">Mô tả</label>
									<textarea id="description" class="form-control" name="description" required id="description" ><?php echo $obj?$obj->description:"";?></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group required">
									<label class="control-label">Nội dung</label>
									<textarea id="content" class="form-control" name="content" required id="content" ><?php echo $obj?$obj->content:"";?></textarea>
								</div>
								<script>
									var editor = CKEDITOR.replace('content',{
										language:'vi',
										filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

										filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
										
										filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
										
										filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
										
										filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
										
										filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

									});
                        		</script>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin');?>">Quay lại</a>
								<button type="reset" class="btn btn-warning">Huỷ</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Lưu</button>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->

	
</div>
<!-- END: .app-main -->