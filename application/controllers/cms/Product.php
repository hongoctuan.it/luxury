<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['product'] = false;
		$this->data['cates'] = $this->M_myweb->set('deleted',0)->set_table('category')->gets();
		$this->Model = $this->M_myweb->set_table('product');
		$this->load->model('cms/m_product');
		$this->load->library('pagination');
	}
	
	public function index()
	{
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "hot":
				$this->hot();
				break;
			case "unhot":
				$this->unHot();
				break;
			case "lock":
				$this->lock();
				break;
			case "unlock":
				$this->unLock();
				break;
			case "del":
				$this->delete();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		// $this->data["results"]="";
		// // init params
		// $this->_limit = 10;
        // $start= (isset($_GET['paging'])) ? $_GET['paging'] : 0;
		// $this->_total = $this->m_product->get_total(); 
        // if ($this->_total > 0) 
        // {            
		// 	$this->data["product"] = $this->m_product->get_current_page_records($this->_limit, $start);
        //     $config['base_url'] = site_url('admin/product');
        //     $this->data["links"] = $this->createLinks($start);
		// }
		$this->data["product"] = $this->m_product->get_current_page_records();
		$this->data['subview'] = 'cms/product/home';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function createLinks($start) {
		
		$page_curent = ceil( $start / $this->_limit )+1;
		$page = ceil($this->_total/$this->_limit);
		$last = ceil( $this->_total / $this->_limit );	
		$html = '<ul>';
		if($page_curent < $page-1){
			$html .= '<li style="list-style:none; float:left"><a class="btn btn-info" style="text-decoration: none;" href="'.site_url('admin/product?paging='.($page_curent)*$this->_limit).'">Sau</a></li>'; 
		}
		for($i=1; $i<=$page; $i++){
			if($page_curent == $i){
				$html .= '<li class="btn" style="color:red;list-style:none; float:left; font-weight: bold;">'.$i.'</li>'; 
			}else{
				$html .= '<li style="list-style:none; float:left"><a class="btn" style="text-decoration: none; font-weight: bold;" href="'.site_url('admin/product?paging='.($i-1)*$this->_limit).'">'.$i.'</a></li>'; 
			}
		}
		if($page_curent != 1){
			$html .= '<li style="list-style:none; float:left"><a class="btn btn-info" style="text-decoration: none;" href="'.site_url('admin/product?paging='.($page_curent-2)*$this->_limit).'">Trước</a></li>'; 
		}
		$html .= '</ul>'; 
		return $html;
	}

	private function edit(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
			$this->data['obj']->image_01 = "";
			$this->data['obj']->image_02 = "";
			$this->data['obj']->image_03 = "";
			$this->data['obj']->image_04 = "";
			$this->data['product_partner']=$this->M_myweb->set_table('product_partner')->set("product_id",$_GET['id'])->gets();
		}
		$this->data['partner']=$this->M_myweb->set_table('partner')->sets(array('active'=>1,'deleted'=>0))->gets();
		$this->data['subview'] = 'cms/product/edit';
		$this->load->view('cms/_main_page',$this->data);
	}
	
	private function hot(){
		if(isset($_GET['id'])){
			$data['id'] = $_GET['id'];
			$data['hot'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/product/home';
		return redirect(site_url('admin/product'));
	}

	private function unHot(){
		if(isset($_GET['id'])){
			$data['id'] = $_GET['id'];
			$data['hot'] = 0;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/product/home';
		return redirect(site_url('admin/product'));
	}

	private function lock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 0;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/product/home';
		return redirect(site_url('admin/product'));
	}

	private function unLock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/product/home';
		return redirect(site_url('admin/product'));
	}

	private function save(){
		$data = $this->input->post();
		$partner=$data['partner'];
		$image_01 = "";
		$image_02 = "";
		$image_03 = "";
		$image_04 = "";
		if($_FILES['image_01']['name']!=""){
			$image_01 = do_upload('avatar','image_01');	
			$data['img1'] = $image_01;				
		}
		if($_FILES['image_02']['name']!=""){
			$image_02 = do_upload('avatar','image_02');		
			$data['img2'] = $image_02;		
		}
		if($_FILES['image_03']['name']!=""){
			$image_03 = do_upload('avatar','image_03');	
			$data['img3'] = $image_03;			
		}
		if($_FILES['image_04']['name']!=""){
			$image_04 = do_upload('avatar','image_04');	
			$data['img4'] = $image_04;			
		}
		if(!isset($data['slug'])||trim($data['slug'])==""){
			$data['slug'] = str_replace(" ","-",stripUnicode($data['name']));
		}else{
			$data['slug'] = str_replace(" ","-",stripUnicode($data['slug']));
		}
		if($this->id){
			$this->Model->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật sản phẩm thành công");
			$this->m_product->deleteRow(array('product_id'=>$this->id),'product_partner');
			foreach($partner as $item)
			{
				$product_partner= array('product_id'=>$this->id,'partner_id' => $item);
				$this->M_myweb->set_table('product_partner')->setPrimary(false)->sets($product_partner)->save();
			}
		}else{
			$id=$this->Model->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Thêm sản phẩm thành công");
			foreach($partner as $item)
			{
				$product_partner= array('product_id'=>$id,'partner_id' => $item);
				$this->M_myweb->set_table('product_partner')->setPrimary(false)->sets($product_partner)->save();
			}
		}
		return redirect(site_url('admin/product'));
	}

	private function delete(){
		if($this->id){
			$getPro = $this->Model->set('id',$this->id)->get();
			if($getPro){
				$this->Model->sets(array('deleted'=>1))->setPrimary($this->id)->save();
				$_SESSION['system_msg'] = messageDialog("div","success","Xoá sản phẩm thành công");
			}else{
				$_SESSION['system_msg'] = messageDialog("div","error","Không thể xoá sản phẩm");
			}
		}
		redirect(site_url('admin/product'));
	}
}