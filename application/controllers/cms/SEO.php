<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SEO extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['seo'] = $this->M_myweb->set_table('SEO')->gets();
		$this->Model = $this->M_myweb->set_table('SEO');

	}
	
	public function index()
	{
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->M_myweb->set('deleted',0);
		$this->data['subview'] = 'cms/SEO/home';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function edit(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
		}
		$this->data['subview'] = 'cms/SEO/edit';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function save(){
		$data = $this->input->post();
		if($this->id){
			$this->Model->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật cấu hình thành công");
		}else{
			$this->Model->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật cấu hình thành công");
		}
		return redirect(site_url('admin/SEO'));
	}
}