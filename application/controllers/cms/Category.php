<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('category');
	}
	
	public function index()
	{
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "del":
				$this->delete();
				break;
			case "child_list":
				$this->child_list();
				break;
			case "lock":
				$this->lock();
				break;
			case "unlock":
				$this->unLock();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data['category'] = $this->Model->set('deleted',0)->set('parent',0)->set_orderby('name')->gets();
		$this->data['subview'] = 'cms/category/home';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function child_list(){
		$this->id = $_GET['id'];
		$this->data['cat_parent'] = $this->Model->set('id',$this->id)->set('active',1)->set('deleted',0)->set_orderby('name')->get();
		$this->data['category'] = $this->Model->set('parent',$this->id)->set('active',1)->set('deleted',0)->set_orderby('name')->gets();
		$this->data['id']=$this->id;
		$this->data['subview'] = 'cms/category/home';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function edit(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
		}
		if(isset($_GET['parent'])){
			$this->data['parent'] = $_GET['parent'];
			$this->data['cat_parent'] = $this->Model->set('id',$_GET['parent'])->set('deleted',0)->set_orderby('name')->get();
		}
		$this->data['subview'] = 'cms/category/edit';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function save(){
		$data = $this->input->post();

		if($this->id){
			$data['slug'] = str_replace(" ","-",stripUnicode($data['name']))."-".$this->id;
			if($_FILES['image_01']['name']!=""){
				$image_01 = do_upload('avatar','image_01');	
				$data['img'] = $image_01;				
			}
			if($_FILES['image_02']['name']!=""){
				$image_02 = do_upload('avatar','image_02');	
				$data['img_large'] = $image_02;	
			}
			$this->Model->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật category thành công");
		}else{
			$this->data['latest'] = $this->Model->sets(array('deleted'=>0,'active'=>1))->set_orderby('id','desc')->get();
			$latest_id=$this->data['latest']->id+1;
			$data['slug'] = str_replace(" ","-",stripUnicode($data['name']))."-".$latest_id;
			if($_FILES['image_01']['name']!=""){
				$image_01 = do_upload('avatar','image_01');	
				$data['img'] = $image_01;				
			}
			if($_FILES['image_02']['name']!=""){
				$image_02 = do_upload('avatar','image_02');	
				$data['img_large'] = $image_02;				
			}
			if(isset($_GET['parent'])){
				if($data['level'] == 1)
					$data['level'] = 2;
				else
					$data['level'] = 1;
				$data['parent'] = $_GET['parent'];
			}else{
				$data['level'] = 0;
			}
			$this->Model->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Thêm category thành công");
		}
		if($this->id){
			return site_url('admin/category?act=upd&id='.$this->id.'&token='.$this->data['infoLog']->token);
		}else{
			return redirect(site_url('admin/category'));
		}
	}

	private function delete(){
		if($this->id){
			$getPro = $this->Model->set('id',$this->id)->get();
			if($getPro){
				$this->Model->sets(array('deleted'=>1))->setPrimary($this->id)->save();
				$_SESSION['system_msg'] = messageDialog("div","success","Xoá category thành công");
			}else{
				$_SESSION['system_msg'] = messageDialog("div","error","Không thể xoá category");
			}
		}
		redirect(site_url('admin/category'));
	}

	private function lock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 0;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/category/home';
		return redirect(site_url('admin/category'));
	}

	private function unLock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/category/home';
		return redirect(site_url('admin/category'));
	}
}