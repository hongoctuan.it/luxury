<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('news');
	}
	
	public function index()
	{
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "del":
				$this->delete();
				break;
			case "lock":
				$this->lock();
				break;
			case "unlock":
				$this->unLock();
				break;
			case "child_list":
				$this->child_list();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data['news'] = $this->Model->set('deleted',0)->set('parent',0)->set_orderby('name')->gets();
		$this->data['subview'] = 'cms/news/home';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function child_list(){
		$this->id = $_GET['id'];
		$this->data['news_cate'] = $this->Model->set('id',$this->id)->set('deleted',0)->set_orderby('name')->get();
		$this->data['news'] = $this->Model->set('parent',$this->id)->set('deleted',0)->set('active',1)->set_orderby('name')->gets();
		$this->data['id']=$this->id;
		$this->data['subview'] = 'cms/news/home';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function edit(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
		}
		if(isset($_GET['parent'])){
			$this->data['parent'] = $_GET['parent'];
		}
		$this->data['news_cate'] = $this->Model->set('parent',0)->set('deleted',0)->set_orderby('name')->gets();
		$this->data['subview'] = 'cms/news/edit';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function save(){
		$data = $this->input->post();
		if(!isset($data['slug'])||trim($data['slug'])==""){
			$data['slug'] = str_replace(" ","-",stripUnicode($data['name']));
		}else{
			$data['slug'] = str_replace(" ","-",stripUnicode($data['slug']));
		}
		if($this->id){
			$image_04 = "";
			if($_FILES['image_01']['name']!=""){
				$image_01 = do_upload('avatar','image_01');	
				$data['img'] = $image_01;				
			}
			$this->Model->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật category thành công");
		}else{
			$image_04 = "";
			if($_FILES['image_01']['name']!=""){
				$image_01 = do_upload('avatar','image_01');	
				$data['img'] = $image_01;				
			}
			if(isset($_GET['parent'])){
				$data['parent'] = $_GET['parent'];
			}
			$this->Model->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Thêm category thành công");
		}
		if($this->id){
			return site_url('admin/news?act=upd&id='.$this->id.'&token='.$this->data['infoLog']->token);
		}else{
			return redirect(site_url('admin/news'));
		}
	}

	private function delete(){
		if($this->id){
			$getPro = $this->Model->set('id',$this->id)->get();
			if($getPro){
				$this->Model->sets(array('deleted'=>1))->setPrimary($this->id)->save();
				$_SESSION['system_msg'] = messageDialog("div","success","Xoá category thành công");
			}else{
				$_SESSION['system_msg'] = messageDialog("div","error","Không thể xoá category");
			}
		}
		return redirect(site_url('admin/news'));
	}

	private function lock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 0;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		if(isset($_GET['parent']))
			return redirect(site_url('admin/news?act=child_list&id='.$_GET['parent'].'&token='.$this->data['infoLog']->token));	
		else
			return redirect(site_url('admin/news'));
	}

	private function unLock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		if(isset($_GET['parent']))
			return redirect(site_url('admin/news?act=child_list&id='.$_GET['parent'].'&token='.$this->data['infoLog']->token));	
		else
			return redirect(site_url('admin/news'));
			
	}
}