<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->data['style']=isset($_GET['style'])?$_GET['style']:false;
		$this->data['by']=isset($_GET['by'])?$_GET['by']:false;
		$this->data['order']=isset($_GET['order'])?$_GET['order']:false;
		$this->cate=isset($_GET['cate'])?$this->input->get('cate'):false;
		// $this->data['cates'] = $this->M_myweb->set_table('category')->gets();
		$this->load->model('default/m_category');
		$this->load->model('default/m_product');
		$this->load->model('default/m_seo');
		$this->data['meta']  = $this->m_seo->getSEO(2);
	}
	public function index()
	{


		$this->data['cats'] = $this->m_category->getProductList();
		if($this->data['by'])
		{
			$this->m_category->setOrderBy($this->data['order'],$this->data['by']);
		}
		$this->data['products']=$this->m_category->loadProductPage();
		$this->m_product->setWhere('hot',1);
		$this->data['hot_products']=$this->m_product->getProducts();
		foreach($this->data['cats'] as $item)
		{
			$this->m_category->setLimit(PHP_INT_MAX);
			$this->m_category->loadProductPage($item->id);
			$this->data['cats'][$item->id]->count=count($this->m_category->loadProductPage($item->id));
		}
		$this->data['subview'] 	= 'default/category/V_category';
		$this->load->view('default/_main_page',$this->data);

	}
	public function category($slug)
	{
		$cates=$this->m_category->getCategoryWhere(array('slug'=>$slug));
		$this->data['category_id']=$cates[0]->id;
		if(!empty($cates))
		{
			$cates = $this->m_category->getCategoryTree($cates[0]->id);
			$this->data['category_tree']=$cates;
			$this->data['cats'] = $this->m_category->getProductList();

			if($this->data['by'])
			{
				$this->m_category->setOrderBy($this->data['order'],$this->data['by']);
			}
			$this->data['products']=$this->m_category->loadProductPage($this->data['category_id']);
			$this->m_product->setWhere('hot',1);

			$this->data['hot_products']=$this->m_product->getProducts();
			foreach($this->data['cats'] as $item)
			{
				$this->m_category->setLimit(PHP_INT_MAX);
				$this->m_category->loadProductPage($item->id);
				$this->data['cats'][$item->id]->count=count($this->m_category->loadProductPage($item->id));
			}

			$this->data['title']	= "Sản Phẩm";
			$this->data['subview'] 	= 'default/category/V_category';
			$this->load->view('default/_main_page',$this->data);
		}else{
			redirect(site_url('danh-muc-san-pham'));
		}

	}
	public function getPageAjax()
	{
		$page = $_POST['page'];
		if(isset($page))
		{
			$category = $_POST['category']!=0?$_POST['category']:false;
			$this->m_category->setPage($page);
			if($_POST['order']&&$_POST['by'])
			{
				$this->m_category->setOrderBy($_POST['order'],$_POST['by']);
			}
			$this->data['products'] = $this->m_category->loadProductPage($category);
			$this->data['style']=$_POST['style'];
			if($this->data['products'])
			{
				$this->load->view('default/category/V_product_page',$this->data);
			}
		}else{
			return false;
		}
	}
}
